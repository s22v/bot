# -*- coding: utf-8 -*-
import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from chat import Chat
from vk_api.keyboard import VkKeyboard
from vk_api.utils import get_random_id
from vk_api import VkUpload
from pd import *
import pandas as pd

chat_object = {}


try:
    print('ok')
    users = pd.read_pickle('data/users.pickle')
except:
    users = pd.DataFrame(columns=['id', 'lastName', 'name', 'username', 'group', 'bio', 'account_type']).set_index('id')


def sent_photo(upload, url):
    try:
        return upload.photo_messages(photos='img/%s.jpg' % url)[0]
    except:
        try:
            return upload.photo_messages(photos='img/%s.png' % url)[0]
        except:
            return upload.photo_messages(photos='img/error.jpg')[0]


def get_keyboard(bnt):
    keyboard = VkKeyboard(one_time=True)
    for row in bnt[:-1]:
        for button in row:
            keyboard.add_button(button['txt'], color=button['color'] if 'color' in button else None)
        keyboard.add_line()

    for button in bnt[-1]:
        keyboard.add_button(button['txt'], color=button['color'] if 'color' in button else None)
    return keyboard


def main():
    vk_session = vk_api.VkApi(token=open('config/token.txt', 'r').read())
    server = VkBotLongPoll(vk_session, '171463266')

    vk = vk_session.get_api()
    upload = VkUpload(vk_session)

    print('\033[01m', 'bot started...', '\033[0m')
    print('-' * 30)
    for event in server.listen():
        if event.type == VkBotEventType.MESSAGE_NEW:
            id = event.object.from_id
            msg = event.obj.text
            if id in chat_object.keys():
                txt, bnt, user = chat_object[id].answer(msg, users.loc[id])
            else:
                users.loc[id] = {'lastName': str(get_user_name_from_vk_id(id).split()[1]),
                                 'name': str(get_user_name_from_vk_id(id).split()[0]),
                                 'username': 'vk.com/' + str(id),
                                 'bio': None,
                                 'group': None,
                                 'account_type': 'user'}
                users.to_pickle('data/users.pickle')
                chat_object[id] = Chat()
                txt, bnt, user = chat_object[id].answer(msg, users.loc[id])

            if bnt:
                keyboard = get_keyboard(bnt)
            users.loc[id] = user
            users.to_pickle('data/users.pickle')
            vk.messages.send(
                random_id=get_random_id(),
                user_id=id,
                keyboard=keyboard.get_keyboard() if bnt else keyboard.get_empty_keyboard(),
                # attachment='photo{}_{}'.format(photo['owner_id'], photo['id']) if url else None,
                message=txt
            )

            print('\033[01m', 'Username:', '\033[0m', get_user_name_from_vk_id(id))
            print('\033[01m', 'User msg:', '\033[0m', msg)
            print('\033[01m', 'Bot msg:', '\033[0m', txt)
            print('-' * 30)


if __name__ == '__main__':
            main()

