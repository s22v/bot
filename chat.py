from rq import *
from vk_api.keyboard import VkKeyboardColor

CLR = VkKeyboardColor.DEFAULT
NEG = VkKeyboardColor.NEGATIVE
POS = VkKeyboardColor.POSITIVE

main_buttons = [[{'txt': '✍ Домашнее задание', 'color': CLR},
                 {'txt': '📆 Расписание', 'color': CLR}],
                [{'txt': '⚙️ Настройки', 'color': CLR}]]

set_buttons = [[{'txt': '✏️Изменить класс', 'color': CLR},
                {'txt': '📋 INFO', 'color': CLR}],
               [{'txt': 'Выход', 'color': NEG}]]


def to_buttons(buttons_list, n=4, exit_button=False):
    buttons = []
    for i in range(len(buttons_list)):
        if i % n == 0:
            buttons.append([])
        buttons[-1].append({"txt": buttons_list[i], "color": CLR})
    if exit_button:
        buttons.append([{'txt': 'Выход', 'color': NEG}])
    return buttons


class Chat:
    def __init__(self):
        self.group = None
        self.class_list = []
        self.class_buttons = None
        self.subject_list = []
        self.subject_buttons = None
        self.action = ""

    def answer(self, message):
        if message in ['Выход']:
            self.action = 'main'
            return ["Выберите опцию"], main_buttons

        if self.action == "unknown_class":
            if message in self.class_list:
                self.group = message
                self.action = "main"
                return ["Сохранено"], main_buttons
            else:
                return ["Выберите класс из списка"], self.class_buttons

        if not self.group:
            self.class_list = get_classes_list()
            self.class_buttons = to_buttons(self.class_list, 3)
            self.action = "unknown_class"
            return ["Выберите свой класс"], self.class_buttons

        if self.action == "main" or message == 'бот':
            if message == '✍ Домашнее задание':
                self.action = "hw"
                self.subject_list = get_subjects(self.group)
                self.subject_buttons = to_buttons(self.subject_list, 2, True)
                return ['Выберите предмет'], self.subject_buttons

            if message == "📆 Расписание":
                return [get_schedule(self.group)], main_buttons

            if message == '⚙️ Настройки':
                self.action = "settings"
                return ["Настройки:"], set_buttons
            return ["Выберите опцию из списка"], main_buttons

        if self.action == "hw":
            if message in self.subject_list:
                return get_homeworks(self.group, message), self.subject_buttons
            else:
                return ['Название придмета введено некорректно. Чтобы выйти, нажмите Выход'], self.subject_buttons

        if self.action == "settings":
            if message == '✏️Изменить класс':
                self.class_list = get_classes_list()
                self.class_buttons = to_buttons(self.class_list, 3)
                self.action = "unknown_class"
                return ["Выберите свой класс"], self.class_buttons
            if message == '📋 INFO':
                return ["""Главный администратор группы Хроники 179 - Миронов Валерий (@mironovval).
                К нему стоит обращаться, если вы хотите стать администратором бота для своего класса.
                Разработчики - @alexdat1979 и @cher_v_a - к ним обращаться по всем остальным вопросам касательно бота"""], set_buttons
            return ["Выберите опцию из списка"], set_buttons

        return [], None
