import io
import pandas as pd
import requests
from vk_api.keyboard import VkKeyboardColor

CLR = VkKeyboardColor.DEFAULT


def get_table_url(group, type):
    url_table = open("config/url_table.txt", "r").read()
    s = requests.get(url_table).content
    df = pd.read_csv(io.StringIO(s.decode('utf-8')))
    df = df.set_index("group")
    return df.loc[group, type]


def get_schedule(group):
    s = requests.get(get_table_url(group, "SH")).content
    df = pd.read_csv(io.StringIO(s.decode('utf-8')))
    ans = ""
    for day in df.columns:
        ans += day + ":\n"
        subj = list(df[day].astype(str))
        i = 0
        while i < len(subj) and subj[i] != "nan":
            ans += str(i + 1) + ". " + subj[i] + "\n"
            i += 1
        ans += "\n\n"
    return ans


def get_homeworks(group, subject):
    s = requests.get(get_table_url(group, "HW")).content
    df = pd.read_csv(io.StringIO(s.decode('utf-8')))
    df = df.astype(str)
    df = df.loc[df.subject == subject, ['text', 'date']]
    df['ans'] = df['date'] + '\n' + df['text']
    if list(df.ans):
        return list(df.ans)
    return ['No Homework for you, guys']


def get_subjects(group):
    s = requests.get(get_table_url(group, "HW")).content
    df = pd.read_csv(io.StringIO(s.decode('utf-8')))
    df = df.dropna()
    return list(set(df.subject.astype(str)))


def get_classes_list():
    s = requests.get(open("config/url_table.txt", "r").read()).content
    df = pd.read_csv(io.StringIO(s.decode('utf-8')))
    df = df.dropna()
    return list(set(df.group.astype(str)))
