from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType
from chat import Chat
from vk_api.keyboard import VkKeyboard
from vk_api.utils import get_random_id
from vk_api import VkApi
import pickle
import sys
import time

CHECKS_PER_SECOND = 5
PSEUDO_SUDOERS = [258791397, 329152246, 449359458]
GROUP_ID = '171935851'

""""
def sent_photo(upload, url):
    try:
        return upload.photo_messages(photos='img/%s.jpg' % url)[0]
    except:
        try:
            return upload.photo_messages(photos='img/%s.png' % url)[0]
        except:
            return upload.photo_messages(photos='img/error.jpg')[0]
"""


def convert_to_keyboard(bnt):
    keyboard_object = VkKeyboard(one_time=False)
    for row in bnt[:-1]:
        for button in row:
            keyboard_object.add_button(button['txt'], color=button['color'] if 'color' in button else None)
        keyboard_object.add_line()

    for button in bnt[-1]:
        keyboard_object.add_button(button['txt'], color=button['color'] if 'color' in button else None)
    return keyboard_object


def pickling(my_little_set):
    with open('data/chat_object.pickle', 'wb') as handle:
        pickle.dump(my_little_set, handle, protocol=pickle.HIGHEST_PROTOCOL)


def main():
    vk_session = VkApi(token=open('config/token.txt', 'r').read())
    server = VkBotLongPoll(vk_session, GROUP_ID)
    after_party_action = ""

    try:
        with open('data/chat_object.pickle', 'rb') as handle:
            chat_object = pickle.load(handle)
            print('Chat object were installed')
    except FileNotFoundError:
        chat_object = {}

    vk = vk_session.get_api()
    # upload = VkUpload(vk_session)

    print('bot started at ', time.time())
    print('-' * 30)
    for event in server.listen():
        time.sleep(1 / CHECKS_PER_SECOND)
        if event.type != VkBotEventType.MESSAGE_NEW:
            continue
        id = event.object.from_id
        msg = event.object.text
        keyboard = []

        if id in chat_object.keys():
            txt_list, bnt = chat_object[id].answer(msg)
        else:
            chat_object[id] = Chat()
            txt_list, bnt = chat_object[id].answer(msg)

        if bnt:
            keyboard = convert_to_keyboard(bnt)

        with open('data/chat_object.pickle', 'wb') as handle:
            pickle.dump(chat_object, handle, protocol=pickle.HIGHEST_PROTOCOL)

        if msg == 'suka shutdown':
            if id in PSEUDO_SUDOERS:
                print('shutDown from', id, " at ", time.time())
                after_party_action = "sd"
                txt_list = ['shutDown from ' + str(id)]
            else:
                txt_list = ['Access denied']

        if msg == 'suka flush':
            if id in PSEUDO_SUDOERS:
                print('flushed from', id, " at ", time.time())
                chat_object = {}
                pickling(chat_object)
                after_party_action = "rb"
                txt_list = ['flushed from ' + str(id)]
            else:
                txt_list = ['Access denied']

        if txt_list:
            for txt in txt_list:
                vk.messages.send(
                    random_id=get_random_id(),
                    user_id=id,
                    keyboard=keyboard.get_keyboard() if bnt else keyboard.get_empty_keyboard(),
                    message=txt
                )

        if after_party_action == "sd":
            sys.exit(0)
        elif after_party_action == "rb":
            raise Exception


if __name__ == '__main__':
    while True:
        try:
            main()
        except Exception as ex:
            print(ex, " at ", time.time())
